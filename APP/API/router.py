from . import api
from quart import jsonify
from quart import request, render_template, redirect, url_for

@api.route('/geter')
async def geter():
    response =  jsonify({"Header":"Title", "Body":"Content", "Other": "Some other data"})
    return response

@api.route('/puter', methods=['GET','POST'])
async def puter():
    if request.method == 'POST':
        result = await request.values
        print(result)
        return await render_template('index.html', data=result)
    return redirect(url_for("core.index"))