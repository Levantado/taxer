from quart import Quart
from APP.API import api
from APP.core import core
import config

def create_app(conf='Development'):
    app = Quart(__name__, template_folder='template')
    app.config.from_object(f"config.{conf}")
    app.register_blueprint(api, url_prefix='/API')
    app.register_blueprint(core)
    return app

