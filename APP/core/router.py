from . import core
from quart import render_template

@core.route('/')
async def index():
    response = await render_template('index.html')
    return response