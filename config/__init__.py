class Base():
    SECRET = "SOME DATA"

class Development(Base):
    DEBUG = True
    SECRET = '1234567890'

class Production(Base):
    SECRET = "TOP SECRET"